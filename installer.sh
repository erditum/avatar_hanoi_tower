#!/bin/bash

TARGET_FOLDER_NRP_ROS=$HBP

cp -Ru GazeboRosPackages ${TARGET_FOLDER_NRP_ROS}
cp -Ru Models ${TARGET_FOLDER_NRP_ROS}
cp -Ru Experiments ${TARGET_FOLDER_NRP_ROS}

sudo apt-get update
sudo apt install -y ros-${ROS_DISTRO}-ros-controllers
rosdep install -y towers_of_hanoi_kuka_iiwa_14
rosdep install -y avatar_control_plugin
rosdep install -y kuka_iiwa_14_prismatic_gripper
rosdep install -y kuka_iiwa_14_prismatic_gripper_config
rosdep install -y kuka_iiwa_14_moveit
sudo apt install -y  ros-${ROS_DISTRO}-moveit
sudo apt install -y ros-${ROS_DISTRO}-moveit-visual-tools
cd ${TARGET_FOLDER_NRP_ROS}/GazeboRosPackages
catkin_make

cd ${NRP_MODELS_DIRECTORY}
./create-symlinks.sh